<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use SoftDeletes;
    protected $fillable = ['name','city_id'];

	public function city(){
		return $this->belongsTo('App\City');
    }    
    
}
