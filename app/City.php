<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class City extends Model
{
    use SoftDeletes;
    protected $fillable = ['city','uf'];

    public function clients(){
		return $this->hasMany('App\Client');
    }  
}
