<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use DataTables;


class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $client = new Client($request->all());

        return json_encode(["status"=>$client->save()]);
    }

	public function list(){
        $query = Client::select('id','name','city_id')
            ->with('city')
            ->get();
			return Datatables::of(collect($query))->make(true);
	}


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query = Client::select('id','name','city_id')
            ->with('city')
            ->where('id',$id)
            ->get();

            return json_encode(['data'=>$query]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $_new = (object) $request->all();

        $_old = Client::find($id);
                
        $_old->name = $_new->name;
        $_old->city_id = $_new->city_id;

        return json_encode(['status'=>$_old->save()]);
    }

	public function delete($id){
          $obj = Client::find($id);
                $obj = $obj->delete();
                return  json_encode(['status'=>$obj]);
      }
}
