<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('clients.index');
});

Route::post('clients/create','ClientController@store');
Route::get('clients/list','ClientController@list');
Route::get('clients/edit/{id}','ClientController@edit');
Route::post('clients/update/{id}','ClientController@update');
Route::get('clients/delete/{id}','ClientController@delete');