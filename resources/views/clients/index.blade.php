<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Cadastro de clientes - Teste Neri</title>

        <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet" type="text/css">
        <link rel = 'stylesheet' type = 'text/css' href = 'css/base.min.css' />
        <link rel = 'stylesheet' type = 'text/css' href = 'css/form-validation.min.css' />
        <link rel = 'stylesheet' type = 'text/css' href = 'css/client/client.min.css' />
        <style>
            .b-red{
                border:1px red solid;
            }
            .b-blue{
                border:1px blue solid;
            }
            .b-green{
                border:1px green solid;
            }
        </style>
    </head>
    <body>
        <div class="container h-100 pt-4">
            <h1><i class = 'ico ico-user' style = 'color:brown'></i> Teste CRUD Laravel - Neri</h1>
            <hr />
            <div class="row">
                <div class = 'col-xs-12 col-sm-6 d-flex justify-content-start'>
                    <button id = 'info-edit' class = 'btn btn-warning btn-sm disabled d-none'>
                        <i class = 'ico ico-edit'></i> Editando cliente nº 
                        <span class="badge badge-pill badge-dark">10</span>    
                    </button>
                </div>
                <div class = 'col-xs-12 col-sm-6 d-flex justify-content-end'>
                    <button id = 'btn-new' class = 'btn btn-info btn-sm'><i class = 'ico ico-new'></i> Novo Cliente</button>
                </div>
            </div>
            <hr />
            <div class="row"  style = 'height:80px'>
                <div class = 'col-xs-12 col-sm-12'>
                    @include('clients.form')
                </div>
            </div>
            <hr />
            <div class="row d-flex justify-content-center">
                <div class = 'col-xs-12 col-sm-12'>
                    @include('clients.table-list')
                </div>
            </div>
        </div>

        <script>
            laravel_token = '{{csrf_token()}}'
        </script>
        <!-- scripts -->
        <script assync defer src="{{ asset('js/optimized_cities.js') }}" charset="ISO-8859-1"></script>
        <script src = 'js/jquery.min.js'></script>
        <script src = 'js/popper.min.js'></script>
        <script src = 'js/bootstrap.min.js'></script>
        <script src = 'js/form-validation.min.js'></script>
        <script src = 'js/form-validation-pt_BR.js'></script>
        <script src = 'js/client/client.min.js'></script>
        <script src = 'js/client/client-babel.min.js'></script>
    </body>
</html>
