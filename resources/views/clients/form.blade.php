<form id = 'clients-form' action = 'clients/create' method = 'POST'>
    <div class = 'row'>
        <div class = 'col-xs-3 col-sm-1'>
            <div class = 'form-group'>
                <label for = 'id'><strong>#ID</strong></label>
                <input name = 'id' id = 'id' type = 'text' class = 'form-control' disabled/>
            </div>
        </div>
        <div class = 'col-xs-12 col-sm-5'>
            <div class = 'form-group'>
                <label for = 'name'><strong>Nome do Cliente</strong></label>
                <input id = 'name' name = 'name' type = 'text' class = 'form-control' />
            </div>
        </div>
        <div class = 'col-xs-12 col-sm-4'>
            <div class = 'form-group'>
                <label for = 'city'><strong>Cidade</strong></label>
                <input id = 'city' name = 'city' type = 'text' fv-value = '' class = 'form-control' />
            </div>
        </div>
        <div class = 'col-xs-12 col-sm-2'>
            <div class = 'form-group'>
                <label class = 'w-100'><strong>&nbsp;</strong></label>
                <button type = 'submit' id = 'btn-save' class = 'btn btn-success float-right'><i class = 'ico ico-save'></i> Salvar</button>
                <button type = 'submit' id = 'btn-update' class = 'd-none btn btn-warning float-right'><i class = 'ico ico-save'></i> Atualizar</button>
            </div>
        </div>
    </div>

</form>