$(document).ready(function(){
    $("#city").autocomplete({
        maxheight:100,
        minChars:2,
        preserveInput:true,
        lookup: $.map($CB, function(item){
          return { value: item.c+' - '+item.u, data: item.i};
        }),
        onSelect:function(sugg,a){
            $(this).val(sugg.value); 
            $(this).attr('fv-value',sugg.data);
            document.fv1.revalidateField('city');
        },
        onInvalidateSelection(){
          $(this).attr('fv-value','');
          document.fv1.revalidateField('city');
        }
      });
     
      $('#btn-new').on('click',()=>{
        document.location.reload();
      });

      document.fv1 = FormValidation.formValidation(
        document.getElementById('clients-form'),
        {
          fields: {
            name:{
              validators:{
                notEmpty:{
                  message: 'O nome do cliente é obrigatório!'
                }
              }
            },
            city:{
              validators:{
                callback:{
                  callback:function(obj){
                    let el = $(obj.element);

                      if(!el.val().length)
                        return {
                          valid:false,
                          message:'O campo cidade é obrigatório'
                        }
                      
                      if(el.attr('fv-value') == '')
                        return {
                          valid:false,
                          message:'cidade inválida!'
                        }
                        
                      return true;
                    }                    
                }
              }
            },
          },
          plugins:{
            trigger: new FormValidation.plugins.Trigger(),
            submitButton: new FormValidation.plugins.SubmitButton(),
            bootstrap: new FormValidation.plugins.Bootstrap(),
            icon: new FormValidation.plugins.Icon({
              valid: 'fv-ico ico-check',
              invalid: 'fv-ico ico-close',
              validating: 'fv-ico ico-gear ico-spin'
            }),
          },
      }).setLocale('pt_BR', FormValidation.locales.pt_BR)
      .on('core.form.valid', function() {
        const isUpdate = $('#clients-form').attr('action') !== 'clients/create';

        $.ajax({ 
          url: $('#clients-form').attr('action'),
          headers: {'X-CSRF-Token': laravel_token},
          method: 'POST',
          data: {
            "name":$('#name').val(),
            "city_id":$('#city').attr('fv-value')
          },
          beforeSend: function(){
              HoldOn.open({message:`${isUpdate ? 'Atualizando' : 'Salvando'} dados, aguarde...`,theme:'sk-bounce'});
          },
          success: function(data){
            let _data = JSON.parse(data);

            if(_data.status){
              HoldOn.close();
              swal({
                title:`Cliente ${isUpdate ? 'atualizado' : 'cadastrado'} com sucesso!`,
                confirmButtonText:'OK',
                type:"success",
                onClose:function(){
                  location.reload();
                }
              });
            }

          },
          error:function(ret){
            swal({
              title:`Ocorreram problemas o cliente não pode ser ${isUpdate ? 'atualizado' : 'cadastrado'}!`,
              confirmButtonText:'OK',
              type:"error",
              onClose:function(){
              }
            });
          }
        });

      });
      

      document.dt = $('#clients-table').DataTable({
        aaSorting:[ [0,"desc" ]],
        ajax: 'clients/list',
        pageLength: 6,
        initComplete:function(){
          //parent call
          let api = this.api();
          $.fn.dataTable.defaults.initComplete(this);
        },
        footerCallback:function(row, data, start, end, display){
        },
        columns: [
          { data: 'id', name: 'id'},
          { data: 'name', name: 'name'},
          { data: null, name: 'city'},
          { data: 'actions', name: 'actions'},
        ],
        columnDefs:
        [
          {targets:'__dt_',width: "3%",class:"text-center",searchable: true,orderable:true},
          {targets:'__dt_name',searchable: true,orderable:true},
          {targets:'__dt_cidade',searchable: true,orderable:true,render:function(data,type,row){
              return `${data.city.city} - ${data.city.uf}`
            }
          },
          {targets:'__dt_acoes',width:"7%",className:"text-center",searchable:false,orderable:false,
            render:function(data,type,row,y){
              return document.dt.addDTButtons({
                buttons:[
                  {ico:'ico-edit',_class:'text-info',title:'editar'},
                  {ico:'ico-trash',_class:'text-danger',title:'excluir'},
              ]});
            }
          }
        ]	
      }).on('click',".btn-dt-button[data-original-title=editar]",function(){
        var data = document.dt.row($(this).parents('tr')).data();
        view(data);
      }).on('click','.ico-trash',function(){
        var data = document.dt.row($(this).parents('tr')).data();
        _delete(data.id);
      }).on('draw.dt',function(){
        $('[data-toggle="tooltip"]').tooltip();
      });      

});


function view(data){
  $('#info-edit').removeClass('d-none').find('.badge.badge-pill').text(data.id);
  $('#clients-form').attr('action',`clients/update/${data.id}`);
  $('#btn-update').removeClass('d-none');
  $('#btn-save').addClass('d-none');
  $('#id').val(data.id);
  $('#name').val(data.name);
  $("#city").val(`${data.city.city} - ${data.city.uf}`).trigger('change');
  document.fv1.revalidateField('city');
}


function _delete(id){
  swal.queue([{
    title:"Excluir Cliente?",
    html:"Ao executar esta ação <b>todas as informações vinculadas a este cliente serão perdidas</b>, confirma a exclusão?",
    type:"question",
    confirmButtonText:"<i class = 'ico ico-thumbs-up'></i> Sim, confirmo",
    cancelButtonText:"<i class = 'ico ico-thumbs-down'></i> Não, cancelar",
    showCancelButton: true,
    reverseButtons:true,
    showLoaderOnConfirm: true,
    preConfirm: function(){
      return new Promise(function (resolve) {
        $.get(`clients/delete/${id}`)
          .done(function (data){
            let ret = JSON.parse(data);
            if(ret.status == true){
              swal.insertQueueStep({
                title:"Cliente excluído!",
                html:"O cliente <b>"+(id)+"</b> foi excluído do sistema!",
                type:"success"
              })
            }
            else
              swal.insertQueueStep({
                  title:"Ocorreram problemas, o cliente não pode ser removido!",
                    type:"error",
              });
              const isUpdate = $('#clients-form').attr('action') !== 'clients/create';
              if(!isUpdate){
                document.dt.ajax.reload();
                document.dt.draw(true);
              }
              else
                document.location.reload();
              
            resolve();
          })
          .fail(function(ret) {
            resolve();
          })
      })
    }
  }]);
}