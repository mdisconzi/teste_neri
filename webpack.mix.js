const mix = require('laravel-mix');
const MergeIntoSingleFilePlugin = require('webpack-merge-and-include-globally');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


 //create base files

mix.babel([
    'node_modules/jquery/dist/jquery.min.js',
], 'public/js/jquery.min.js');

mix.scripts([
    'node_modules/bootstrap/dist/js/bootstrap.min.js',
], 'public/js/bootstrap.min.js');

mix.copy('node_modules/popper.js/dist/umd/popper.min.js', 'public/js/popper.min.js');  //babel off


mix.styles([
    'node_modules/bootstrap/dist/css/bootstrap.min.css',
],'public/css/base.min.css');

//create specific files for modules

mix.copy('resources/marcelo/js/optimized_cities.js', 'public/js/optimized_cities.js');

mix.scripts([
    'node_modules/devbridge-autocomplete/dist/jquery.autocomplete.min.js',
    'node_modules/sweetalert2/dist/sweetalert2.min.js',
    'resources/marcelo/js/defaults/def-sweetalert2.js',
], 'public/js/client/client.min.js');

mix.babel([
  'resources/marcelo/vendors/dv-holdon/src/js/HoldOn.min.js',
  'node_modules/datatables.net/js/jquery.dataTables.js',
  'resources/marcelo/js/extensions/ext-datatables.js',
  'resources/marcelo/js/defaults/def-datatables.js',
  'resources/marcelo/js/client.js',
], 'public/js/client/client-babel.min.js');


mix.copy('resources/marcelo/vendors/datatables/datatables-pt-br.json','public/js/datatables/datatables-pt-br.json')


mix.styles([
    'node_modules/datatables.net-bs4/css/dataTables.bootstrap4.css',
    'resources/marcelo/vendors/datatables/datatables-default.css',
    'resources/marcelo/vendors/datatables/datatables-pagination.css',
    'resources/marcelo/css/datatables.css',
    'resources/marcelo/vendors/glyphter-font/css/intranetone.css',
    'resources/marcelo/vendors/glyphter-font/css/custom-gi-intranetone.css',
    'resources/marcelo/vendors/dv-holdon/src/css/HoldOn.min.css',
    'resources/marcelo/vendors/glyphter-font/glyphter-font.css',
    'node_modules/sweetalert2/dist/sweetalert2.min.css',
    'resources/marcelo/css/custom-devbridge.css',
    'resources/marcelo/css/custom-holdon.css',
    'resources/marcelo/css/client.css',
], 'public/css/client/client.min.css');





//form validation

mix.styles([
    'resources/marcelo/vendors/formvalidation-dist-v1.3.0/dist/css/formValidation.min.css',
    'resources/marcelo/css/custom-form-validation.css',
  ], 'public/css/form-validation.min.css');


  mix.copyDirectory('resources/marcelo/vendors/glyphter-font/fonts/','public/fonts');


  mix.webpackConfig({ plugins:[
    new MergeIntoSingleFilePlugin({
        files:{
          'js/form-validation.min.js': [
            'node_modules/es6-shim/es6-shim.min.js',
            'resources/marcelo/vendors/formvalidation-dist-v1.3.0/dist/js/FormValidation.full.min.js',
            'resources/marcelo/vendors/formvalidation-dist-v1.3.0/dist/js/plugins/Bootstrap.min.js',
          ]
        },
      }),
      new MergeIntoSingleFilePlugin({
        files:{
          'js/form-validation-pt_BR.js': [
            'resources/marcelo/vendors/formvalidation-dist-v1.3.0/dist/js/locales/pt_BR.js',
          ]
        },
      })
  ]});
